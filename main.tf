provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
  version = "~> 2.35"
}

terraform {
  required_version = ">= 0.12.20"
  backend "s3" {}
}

locals {
  base_name = "${lower(var.owner)}-${lower(var.project)}${var.product != "" ? format("-%s", lower(var.product)) : ""}-${lower(var.environment)}"
  base_tags = {
    Deployer = "Terraform"
    Owner = title(var.owner)
    Project = title(var.project)
  }

  public_subnets = var.public_subnets == 0 ? [] : [
    for public_subnet_index in range(var.public_subnets):
      cidrsubnet(var.cidr_block, var.newbits, public_subnet_index)
  ]

  private_subnets = var.private_subnets == 0 ? [] : [
    for private_subnet_index in range(length(local.public_subnets), length(local.public_subnets) + var.private_subnets):
      cidrsubnet(var.cidr_block, var.newbits, private_subnet_index)
  ]

  custom_subnets = var.custom_subnets == 0 ? [] : [
    for custom_subnet_index in range(length(local.public_subnets) + length(local.private_subnets), length(local.public_subnets) + length(local.private_subnets) + var.custom_subnets):
      cidrsubnet(var.cidr_block, var.newbits, custom_subnet_index)
  ]

  base_route = {
    cidr_block                = "0.0.0.0/0"
    egress_only_gateway_id    = ""
    gateway_id                = ""
    instance_id               = ""
    ipv6_cidr_block           = ""
    nat_gateway_id            = ""
    network_interface_id      = ""
    transit_gateway_id        = ""
    vpc_peering_connection_id = ""
  }

  availability_zones = length(data.aws_availability_zones.available.names) > var.public_subnets ? slice(data.aws_availability_zones.available.names, 0, var.public_subnets) : data.aws_availability_zones.available.names
}

###[ VPC ]#####################################################

resource "aws_vpc" "tm_vpc" {
  cidr_block            = var.cidr_block
  enable_dns_support    = var.enable_dns_support
  enable_dns_hostnames  = var.enable_dns_hostnames
  instance_tenancy      = var.instance_tenancy

  tags = merge({
    "Name" = "${local.base_name}-vpc"
  },
  local.base_tags)
}

###[ SUBNET PUBLIC ]###########################################

resource "aws_subnet" "tm_public_subnet" {
  count             = length(local.public_subnets) 
  vpc_id            = aws_vpc.tm_vpc.id
  cidr_block        = local.public_subnets[count.index]
  availability_zone = local.availability_zones[count.index % length(local.availability_zones)]

  tags = merge({
    "Name" = "${local.base_name}-subnet-${var.public_subnets_identifier}-${count.index}-${local.availability_zones[count.index % length(local.availability_zones)]}"
  },
  local.base_tags)

  depends_on = [aws_vpc.tm_vpc]

  lifecycle {
    ignore_changes = [
      tags
    ]
  }
}

###[ SUBNET PRIVATE ]##########################################

resource "aws_subnet" "tm_private_subnet" {
  count             = length(local.private_subnets)
  vpc_id            = aws_vpc.tm_vpc.id
  cidr_block        = local.private_subnets[count.index]
  availability_zone = local.availability_zones[count.index % length(local.availability_zones)]

  tags = merge({
    "Name" = "${local.base_name}-subnet-${var.private_subnets_identifier}-${count.index}-${local.availability_zones[count.index % length(local.availability_zones)]}"
  },
  local.base_tags)

  depends_on = [aws_vpc.tm_vpc]

  lifecycle {
    ignore_changes = [
      tags
    ]
  }
}

###[ SUBNET CUSTOM ]###########################################

resource "aws_subnet" "tm_custom_subnet" {
  count             = length(local.custom_subnets)
  vpc_id            = aws_vpc.tm_vpc.id
  cidr_block        = local.custom_subnets[count.index]
  availability_zone = local.availability_zones[count.index % length(local.availability_zones)]

  tags = merge({
    "Name" = "${local.base_name}-subnet-custom-${count.index}-${local.availability_zones[count.index % length(local.availability_zones)]}"
  },
  local.base_tags)

  depends_on = [aws_vpc.tm_vpc]

  lifecycle {
    ignore_changes = [
      tags
    ]
  }
}

###[ INTERNET GATEWAY ]########################################

resource "aws_internet_gateway" "tm_internet_gateway" {
  count = length(aws_subnet.tm_public_subnet) == 0 ? 0 : 1

  vpc_id = aws_vpc.tm_vpc.id

  tags = merge({
    "Name" = "${local.base_name}-internetgateway"
  },
  local.base_tags)

  depends_on = [aws_subnet.tm_public_subnet]
}

###[ ELASTIC IP ]##############################################

resource "aws_eip" "tm_eip" {
  count = length(local.availability_zones) <= length(aws_subnet.tm_public_subnet) ? length(local.availability_zones) : length(aws_subnet.tm_public_subnet)
  vpc = true
  
  tags = merge({
    "Name" = "${local.base_name}-eip-${count.index}-${aws_subnet.tm_public_subnet[count.index].availability_zone}"
  },
  local.base_tags)

  depends_on = [aws_subnet.tm_public_subnet]
}

###[ NAT GATEWAY ]#############################################

resource "aws_nat_gateway" "tm_nat_gateway" {
  count = length(aws_eip.tm_eip)
  allocation_id = aws_eip.tm_eip[count.index].id
  subnet_id = aws_subnet.tm_public_subnet[count.index].id

  tags = merge({
    "Name" = "${local.base_name}-natgateway-${count.index}-${aws_subnet.tm_public_subnet[count.index].availability_zone}"
  },
  local.base_tags)

  depends_on = [aws_subnet.tm_public_subnet, aws_eip.tm_eip]
}

###[ ROUTE TABLE PUBLIC ]######################################

resource "aws_route_table" "tm_public_route_table" {
  count = length(aws_internet_gateway.tm_internet_gateway)
  vpc_id = aws_vpc.tm_vpc.id
  route = concat(
    [
      merge(
        {cidr_block = "0.0.0.0/0"}, 
        {
          gateway_id = aws_internet_gateway.tm_internet_gateway[count.index].id
        }
      )
    ]
  )

  tags = merge({
    "Name" = "${local.base_name}-routetable-public"
  },
  local.base_tags)

  depends_on = [aws_vpc.tm_vpc, aws_internet_gateway.tm_internet_gateway]
}

###[ ROUTE TABLE PRIVATE ]#####################################

resource "aws_route_table" "tm_private_route_table" {
  count = length(aws_nat_gateway.tm_nat_gateway)
  vpc_id = aws_vpc.tm_vpc.id
  route = concat(
    [
      merge(
        {cidr_block = "0.0.0.0/0"}, 
        {
          nat_gateway_id = aws_nat_gateway.tm_nat_gateway[count.index].id
        }
      )
    ]
  )

  tags = merge({
    "Name" = "${local.base_name}-routetable-private-${aws_subnet.tm_public_subnet[count.index].availability_zone}"
  },
  local.base_tags)

  depends_on = [aws_vpc.tm_vpc, aws_nat_gateway.tm_nat_gateway]
}

###[ ROUTE TABLE ASSOCIATIONS ]################################

resource "aws_route_table_association" "tm_public_route_table_association" {
  count = length(aws_subnet.tm_public_subnet)
  subnet_id      = aws_subnet.tm_public_subnet[count.index].id
  route_table_id = aws_route_table.tm_public_route_table[count.index % length(aws_route_table.tm_public_route_table)].id

  depends_on = [aws_subnet.tm_public_subnet, aws_route_table.tm_public_route_table]
}

resource "aws_route_table_association" "tm_private_route_table_association" {
  count = length(aws_subnet.tm_private_subnet)
  subnet_id      = aws_subnet.tm_private_subnet[count.index].id
  route_table_id = aws_route_table.tm_private_route_table[count.index % length(aws_route_table.tm_private_route_table)].id

  depends_on = [aws_subnet.tm_private_subnet, aws_route_table.tm_private_route_table]
}