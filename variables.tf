variable "aws_region" {
  type = string
  default = "sa-east-1"
}

variable "aws_profile" {
  type = string
}

# 

variable "owner" {
  type = string
  default = ""
}

variable "project" {
  type = string
  default = ""
}

variable "product" {
  type = string
  default = ""
}

variable "environment" {
  type = string
  default = ""
}

# VPC related variables

variable "cidr_block" {
  type = string
}

variable "enable_dns_support" {
  type = bool
  default = true
}

variable "enable_dns_hostnames" {
  type = bool
  default = false
}

variable "instance_tenancy" {
  type = string
  default = "default"
}

# SUBNETS related variables

variable "newbits" {
  type = number
  default = 2
}

variable "map_public_ip_on_launch" {
  type = bool
  default = true
}

variable "public_subnets" {
  type = number
  default = 1
}

variable "public_subnets_identifier" {
  type = string
  default = "public"
}

variable "private_subnets" {
  type = number
  default = 0
}

variable "private_subnets_identifier" {
  type = string
  default = "private"
}

variable "custom_subnets" {
  type = number
  default = 0
}

variable "custom_subnets_identifier" {
  type = string
  default = "custom"
}