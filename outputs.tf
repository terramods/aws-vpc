output "public_subnet_ids" {
  value = aws_subnet.tm_public_subnet[*].id
}

output "private_subnet_ids" {
  value = aws_subnet.tm_private_subnet[*].id
}

output "custom_subnet_ids" {
  value = aws_subnet.tm_custom_subnet[*].id
}