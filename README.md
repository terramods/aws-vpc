# VPC

![Minimal Terraform version: ](https://img.shields.io/badge/Terraform->=%200.12.21-blueviolet)

VPC, as probably you know, is one of the main pillars of almost every AWS infrastructure. So VPC was my choice to begin with this idea of bring to the community as flexible and functional Terraform modules as possible.

Following the above mentioned priciples, i've written a couple of common definitions to the vast majority of the VPCs. Those concepts are described below:

| Concept | Description |
| ------ | ------ |
| Public Subnet | In AWS, a Public Subnet is defined by being associated to a Route Table with an Internet Gateway attached |
| Private Subnet | We refer to a Subnet as Private when it is associated to a Route Table with a Nat Gateway attached |

Almost every VPCs have Public and Private Subnets (with the AWS resource set that ensure those functionalities). So this is the main objective on this module, the capability to create, passing few variables, as many Public and Private Subnets as wanted (respecting subnetting rules), with multi AZ support included.

The most basic infrastructure resulting from the use of this module would look like this (the number of public or private subnets and availability zones may vary):

![Diagram](diagram.png)

Supported variables:

| Variable | Type | Default | Description | 
| ------ | ------ | ------ | ------ |
| cidr_block | string | - | The CIDR block for the VPC |
| enable_dns_support | bool | true | A flag to enable/disable DNS support in the VPC |
| enable_dns_hostnames | bool | false | A flag to enable/disable DNS hostnames in the VPC |
| instance_tenancy | string | default | A tenancy option for instances launched into the VPC |
| newbits | number | 2 | Is the number of additional bits with which to extend the CIDR block. For example, if given a prefix ending in /16 and a newbits value of 4, the resulting subnet address will have length /20. |
| public_subnets | number | 1 | A number of Public Subnets to be created. They will be distributed along the Region available zones. The minor of Public Subnets or Availability Zones for region will define the number of NAT Gateways to be created  |
| private_subnets | number | 0 | A number of Private Subnets to be created. They will be distributed along the Region available zones |